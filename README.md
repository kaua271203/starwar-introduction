# Starwar Introduction

Pequeno código que apresenta um conjunto de textos no formato da introdução do StarWars. 

## Pré-Requisitos:
1. NodeJs (16.0 >).

## Instalação
Rode o seguinte comando para instalar as dependencias: 
```
npm install
```

## Uso
```
npm run start
```

Clique em [http://localhost:3000](http://localhost:3000) para acessar.

## Referencias:
1. [Desenvolvendo Starwar Introdução](https://medium.com/womakerscode/desenvolvendo-a-intro-do-star-wars-com-html-css-javascript-node-js-365e1232a169)
